<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Ejercicio 4</title>
</head>
<body>
	<?php
		$var=24.5;
		echo "<b> Este es el Valor de la variable:</b> ". $var;
		echo "<b> Este es el Tipo de Dato de la variable:</b> ". gettype($var). "<br><br>";
		
		$var="HOLA";
		echo "<b> Este es el Valor de la variable:</b> ". $var;
		echo "<b> Este es el Tipo de Dato de la variable:</b> ". gettype($var). "<br><br>";
		
		settype($var, "integer");
		echo "<b>var_dump(var):</b> ";
		var_dump($var);
	?>
</body>
</html>