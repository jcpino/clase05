<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		table {
		  margin: 0 auto;
		}
		table, th, td {
		  border: 1px solid black;
		  border-collapse: collapse;
		}
		td {
			text-align: center;
		}
		.tabla tr:nth-child(even) {
			background-color: Grey;
		}
	</style>

	<title>Ejercicio 5</title>
</head>
<body>
	<table class="tabla">
			<tr>
				<th>Números</th>
				<th>Resultados</th>
			</tr>
			<?php
				for ($i = 0; $i<= 10; $i++) {
					echo "<tr>";
					echo "<td>".$i." * 9 </td>";
					echo "<td>".$i * 9 ."</td>";
					echo "</tr>";
				}
			?>
	</table>
</body>
</html>